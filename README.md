# A la comprapp: Privacy Friendly Shopping List

This app is a fork from [The Privacy Friendly App Shopping List](https://github.com/SecUSo/privacy-friendly-shopping-list)

Same UX but different features and codebase architecture 

Android application that does not require any permissions from the user in order to be installed. 
This app allows to manage shopping lists. Basically users will be able to add, edit and remove lists. A shopping list contains products which can be added, edited and removed as well.

## Improvement over original app

_Work in progress_ check [open issues](https://gitlab.com/jdvr/a-la-comprapp/-/issuesgst) to make an idea 

## Install

it available on Google Play and F-Droid soon. Meanwhile manually build using android studio.


## Motivation

Nowadays there are many apps that require many or all permissions available in Android in order to be installed. However these apps do not always need all of the permissions they ask for. With this project we want to offer an app where the user can be sure that private information such as contacts, location, identity etc., are not being used by the application. This app is part of the Privacy Friendly Apps group develop by Technische Universität Darmstadt, Germany. More information can be found under https://secuso.org/pfa

## Building

The app can be installed by using Android Studio (latest version will be fine).

1. Download content or clone this repository using git
2. Open with Android Studio

### API Reference

Check [app/build.grade file](/app/build.gradle)

## License (Inheritance from original)

Copyright 2016 by Grebiel J. Ifill B. and Christian König

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

The icons used in the nagivation drawer are licensed under the [CC BY 2.5] (http://creativecommons.org/licenses/by/2.5/). In addition to them the app uses icons from [Google Design Material Icons](https://design.google.com/icons/index.html) licensed under Apache License Version 2.0. All other images (the logo of Privacy Friendly Apps, the SECUSO logo, the app logo and the splash screen icon) copyright [Technische Universtität Darmstadt] (www.tu-darmstadt.de) (2016). The logo used for the notifications is part of the main logo of Privacy Friendly Shopping List.

[Read more](LICENSE)

## Contributors

- Juan D. Vega<jdvr>

## Original App Contributors

App-Icon: <br/>
Markus Hau<br/>

Github-Users: <br/>
Ifill-Brito<br/>
Chris<br/>
Christopher Beckmann<br/>
Karola Marky<br/>
Peter Mayer
