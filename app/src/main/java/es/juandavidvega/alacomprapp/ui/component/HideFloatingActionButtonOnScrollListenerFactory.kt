package es.juandavidvega.alacomprapp.ui.component

import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.OnScrollListener
import com.google.android.material.floatingactionbutton.FloatingActionButton

fun createHideFloatingActionButtonOnScrollListener(floatingActionButton: FloatingActionButton): OnScrollListener =
    object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)

            if (dy > 0 && floatingActionButton.isShown) {
                floatingActionButton.hide()
            } else if (dy < 0 && !floatingActionButton.isShown) {
                floatingActionButton.show()
            }
        }
    }