package es.juandavidvega.alacomprapp.ui.home

import es.juandavidvega.alacomprapp.domain.model.ShoppingList

fun ShoppingList.asSummary(): ShoppingListSummary =
    ShoppingListSummary(name, count(), pendingProducts(), total(), checkedTotal(), count() - pendingProducts())

fun ShoppingListSummary.asShoppingList(): ShoppingList =
    ShoppingList(name, emptyList())