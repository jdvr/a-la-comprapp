package es.juandavidvega.alacomprapp.ui.shoppinglist

import es.juandavidvega.alacomprapp.domain.model.ShoppingList

fun ShoppingList.asCurrentShoppingList(): CurrentShoppingListModel =
    CurrentShoppingListModel(
        this.name,
        this.products().filter { it.checked },
        this.products().filter { !it.checked }
    )