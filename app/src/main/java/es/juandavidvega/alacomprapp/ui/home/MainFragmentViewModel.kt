package es.juandavidvega.alacomprapp.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import es.juandavidvega.alacomprapp.domain.command.list.CreateShoppingListCommand
import es.juandavidvega.alacomprapp.domain.command.list.LoadShoppingListCommand
import es.juandavidvega.alacomprapp.domain.command.response.Result
import es.juandavidvega.alacomprapp.persistance.RepositoryFactory

data class ShoppingListSummary(
    val name: String,
    val size: Int,
    val pending: Int,
    val total: Double,
    val currentPrice: Double,
    val checkedProducts: Int
)

class MainFragmentViewModel : ViewModel() {

    private val shoppingListRepository = RepositoryFactory.createShoppingListRepository()

    fun getSummaries(): LiveData<List<ShoppingListSummary>> {
        val summaries = MutableLiveData<List<ShoppingListSummary>>().apply {
            // TODO Manage this inside the command
            value = shoppingListRepository.all().map { it.asSummary() }
        }

        LoadShoppingListCommand(shoppingListRepository)
            .execute()
            .map { it.asSummary() }
            .doOnNext {
                summaries.value = summaries.value?.plus(it)
            }
            .subscribe()
        return summaries
    }

    fun onCreateList(name: String): MutableLiveData<Result> {
        val resultWrapper = MutableLiveData<Result>()
        CreateShoppingListCommand(shoppingListRepository)
            .execute(name, emptyList()) {
                resultWrapper.value = it
            }
        return resultWrapper
    }

    companion object {
        const val LOGGER_TAG = "MainFragmentVM"
    }
}