package es.juandavidvega.alacomprapp.ui.home

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import es.juandavidvega.alacomprapp.R


import es.juandavidvega.alacomprapp.ui.home.MainFragment.OnShoppingListSummaryTappedListener
import es.juandavidvega.alacomprapp.ui.dummy.DummyContent.DummyItem

import kotlinx.android.synthetic.main.fragment_shopping_list_summary.view.*

/**
 * [RecyclerView.Adapter] that can display a [DummyItem] and makes a call to the
 * specified [OnShoppingListSummaryTappedListener].
 * TODO: Replace the implementation with code for your data type.
 */
class MainFragmentRecyclerViewAdapter(
    initialValues: List<ShoppingListSummary>,
    private val onShoppingListSummaryTappedListener: OnShoppingListSummaryTappedListener?
) : RecyclerView.Adapter<MainFragmentRecyclerViewAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener
    var items = initialValues
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as ShoppingListSummary
            // Notify the active callbacks interface (the activity, if the fragment is attached to
            // one) that an item has been selected.
            onShoppingListSummaryTappedListener?.onTapped(item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_shopping_list_summary, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.fillWith(item)
        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = items.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        private val tvName: TextView = mView.tv_list_name
        private val tvQuantity: TextView = mView.tv_quantity
        private val tvPrice: TextView = mView.tv_price

        fun fillWith(shoppingListSummary: ShoppingListSummary) {
            tvName.text = shoppingListSummary.name
            tvQuantity.text = mView.context.getString(
                R.string.shopping_list_summary_quantity_fraction,
                shoppingListSummary.checkedProducts,
                shoppingListSummary.size
            )
            tvPrice.text = mView.context.getString(
                R.string.shopping_list_summary_price_fraction,
                shoppingListSummary.currentPrice,
                shoppingListSummary.total
            )
        }
    }
}
