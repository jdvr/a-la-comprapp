package es.juandavidvega.alacomprapp.ui.shoppinglist

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import es.juandavidvega.alacomprapp.domain.command.list.AddProductToShoppingListCommand
import es.juandavidvega.alacomprapp.domain.command.list.CheckShoppingListProductCommand
import es.juandavidvega.alacomprapp.domain.model.Product
import es.juandavidvega.alacomprapp.domain.model.ShoppingCardProduct
import es.juandavidvega.alacomprapp.domain.model.ShoppingList
import es.juandavidvega.alacomprapp.persistance.RepositoryFactory

data class CurrentShoppingListModel(
    val name: String,
    val checkedProducts: List<ShoppingCardProduct>,
    val uncheckedProducts: List<ShoppingCardProduct>
)

class ShoppingListActivityViewModel : ViewModel() {

    private val shoppingListRepository = RepositoryFactory.createShoppingListRepository()
    private val productRepository = RepositoryFactory.createProductRepository()
    private val liveDataDisplayModel = MutableLiveData<CurrentShoppingListModel>()

    fun getCurrentShoppingList(name: String): LiveData<CurrentShoppingListModel> {
        liveDataDisplayModel.value =
            shoppingListRepository.findByName(name)?.asCurrentShoppingList()
        return liveDataDisplayModel
    }

    fun getAllProducts(): LiveData<List<Product>> {
        // TODO Manage query initial + subscription on a Command/UseCase
        val products = MutableLiveData<List<Product>>().apply {
            value = productRepository.all()
        }
        productRepository.getAll()
            .doOnNext {
                products.value = products.value!!.plus(it)
            }.subscribe()
        return products
    }

    fun onProductAdded(
        product: Product,
        quantity: Int,
        shoppingList: ShoppingList
    ) {
        AddProductToShoppingListCommand(productRepository, shoppingListRepository)
            .execute(product, quantity, shoppingList.name) {
                Log.i(LOGGER_TAG, "product $product added to ${shoppingList.name}")
                liveDataDisplayModel.value =
                    shoppingListRepository.findByName(shoppingList.name)?.asCurrentShoppingList()
            }
    }

    fun onProductChecked(
        shoppingCardProduct: ShoppingCardProduct,
        shoppingListName: String
    ) {
        CheckShoppingListProductCommand(shoppingListRepository)
            .execute(shoppingCardProduct.product, shoppingListName) {
                Log.i(
                    LOGGER_TAG,
                    "product $shoppingCardProduct mark as checked for $shoppingListName"
                )
            }
    }

    companion object {
        const val LOGGER_TAG = "ShoppingListFragmentVM"
    }
}