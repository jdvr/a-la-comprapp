package es.juandavidvega.alacomprapp.ui.shoppinglist

import es.juandavidvega.alacomprapp.domain.model.ShoppingCardProduct

interface OnProductChangedListener {
    enum class Action {
        EDIT,
        DELETE,
        CHECKED,
        UNCHECKED
    }

    fun onChange(shoppingCardProduct: ShoppingCardProduct, action: Action)
}

class StandaloneOnProductChangedListener(
    private val viewModel: ShoppingListActivityViewModel,
    private val shoppingListName: String,
    private val log: (String) -> Unit
) : OnProductChangedListener {
    override fun onChange(
        shoppingCardProduct: ShoppingCardProduct,
        action: OnProductChangedListener.Action
    ) {
        when (action) {
            OnProductChangedListener.Action.CHECKED -> viewModel.onProductChecked(
                shoppingCardProduct, shoppingListName
            )
            else -> log("For: ${shoppingCardProduct.product.name} requested: $action")
        }
    }
}