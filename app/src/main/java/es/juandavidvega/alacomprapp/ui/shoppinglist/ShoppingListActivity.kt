package es.juandavidvega.alacomprapp.ui.shoppinglist

import android.app.AlertDialog
import android.os.Bundle
import android.text.Editable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import es.juandavidvega.alacomprapp.R
import es.juandavidvega.alacomprapp.domain.model.Product
import es.juandavidvega.alacomprapp.domain.model.ShoppingCardProduct
import es.juandavidvega.alacomprapp.domain.model.ShoppingList
import es.juandavidvega.alacomprapp.ui.component.createHideFloatingActionButtonOnScrollListener


class ShoppingListActivity : AppCompatActivity(), OnProductChangedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shopping_list)

        val name = intent?.extras?.getString(SELECTED_SHOPPING_LIST_NAME)
            ?: throw IllegalArgumentException("Missing $SELECTED_SHOPPING_LIST_NAME parameter")

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        val recyclerView = findViewById<RecyclerView>(R.id.rv_list_of_products)
        val addProductButton =
            findViewById<FloatingActionButton>(R.id.add_product_to_shopping_list_button)

        setSupportActionBar(toolbar)
        title = name
        supportActionBar?.setDisplayHomeAsUpEnabled(true);

        val viewModel = ViewModelProvider.AndroidViewModelFactory(application)
            .create(ShoppingListActivityViewModel::class.java)


        val shoppingListRecyclerViewAdapter = ShoppingListRecyclerViewAdapter(
            emptyList(),
            StandaloneOnProductChangedListener(viewModel, name) {
                Log.i(LOGGER_TAG, it)
            }
        )

        with(recyclerView) {
            layoutManager = LinearLayoutManager(applicationContext)
            adapter = shoppingListRecyclerViewAdapter
            addOnScrollListener(createHideFloatingActionButtonOnScrollListener(addProductButton))
        }


        viewModel.getCurrentShoppingList(name).observe({ lifecycle }) {
            shoppingListRecyclerViewAdapter.items = it.uncheckedProducts.plus(it.checkedProducts)
        }


        viewModel.getAllProducts().observe({ lifecycle }) { products ->
            addProductButton.setOnClickListener { onAddListEvent(viewModel, products, name) }
        }

    }


    private fun onAddListEvent(
        viewModel: ShoppingListActivityViewModel,
        products: List<Product>,
        shoppingListName: String
    ) {
        val dialogView =
            LayoutInflater.from(this).inflate(R.layout.dialog_new_product_form, null)
        val existingProductNames = products.map { it.name }.toTypedArray()
        val etProductPrice = dialogView.findViewById<EditText>(R.id.et_new_product_price)
        val etProductCategory = dialogView.findViewById<EditText>(R.id.et_new_product_category)
        val etProductQuantity = dialogView.findViewById<EditText>(R.id.et_new_product_quantity)
        val etProductName = dialogView.findViewById<AutoCompleteTextView>(R.id.et_new_product_name)
        val adapter = ArrayAdapter<String>(
            this,
            android.R.layout.simple_expandable_list_item_1,
            existingProductNames
        )
        etProductName.setAdapter(adapter)
        etProductName.setOnItemClickListener { _, _, _, _ ->
            products.find { it.name == etProductName.text.toString() }?.also {
                fillNewProductFormWithExistingProduct(dialogView, it)
            }
        }
        etProductName.threshold = 1
        AlertDialog.Builder(this)
            .setView(dialogView)
            .setPositiveButton(R.string.bt_text_save) { dialog, _ ->
                val product = Product(
                    etProductName.text.toString(),
                    etProductPrice.text.toString().toDoubleOrZero(),
                    etProductCategory.text.toString()
                )
                viewModel.onProductAdded(
                    product,
                    etProductQuantity.text.toString().toIntOrOne(),
                    ShoppingList(shoppingListName, emptyList())
                )
                dialog.dismiss()
            }
            .setNegativeButton(android.R.string.cancel) { dialog, _ -> dialog.dismiss() }
            .show()
    }

    private fun fillNewProductFormWithExistingProduct(dialogView: View, product: Product) {
        product.price?.also {
            dialogView.findViewById<EditText>(R.id.et_new_product_price).text =
                Editable.Factory.getInstance().newEditable(it.toString())
        }
        product.category?.also {
            dialogView.findViewById<EditText>(R.id.et_new_product_category).text =
                Editable.Factory.getInstance().newEditable(it)
        }
    }

    override fun onChange(
        shoppingCardProduct: ShoppingCardProduct,
        action: OnProductChangedListener.Action
    ) {
        when (action) {
            OnProductChangedListener.Action.CHECKED -> Log.i(
                LOGGER_TAG,
                "For: ${shoppingCardProduct.product.name} requested: $action"
            )
            else -> Log.i(LOGGER_TAG, "For: ${shoppingCardProduct.product.name} requested: $action")
        }
    }

    companion object {
        const val SELECTED_SHOPPING_LIST_NAME = "selectedShoppingListName"
        const val LOGGER_TAG = "ShoppingListActivity"
    }

}

private fun String.toIntOrOne(): Int =
    toIntOrNull() ?: 1

private fun String.toDoubleOrZero(): Double =
    toDoubleOrNull() ?: .0