package es.juandavidvega.alacomprapp.ui.home

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import es.juandavidvega.alacomprapp.R


/**
 * A fragment representing a list of Items.
 * Activities containing this fragment MUST implement the
 * [ShoppingListDetailFragment.OnListFragmentInteractionListener] interface.
 */
class MainFragment : Fragment() {

    private var itemTappedListener: OnShoppingListSummaryTappedListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_main, container, false)
        val fragmentViewModel = ViewModelProvider.AndroidViewModelFactory(activity!!.application)
            .create(MainFragmentViewModel::class.java)
        val rv = root.findViewById<RecyclerView>(R.id.rv_list_of_list)
        val shoppingListRecyclerViewAdapter = MainFragmentRecyclerViewAdapter(
            emptyList(),
            itemTappedListener
        )
        rv.layoutManager = LinearLayoutManager(context)
        rv.adapter = shoppingListRecyclerViewAdapter
        fragmentViewModel.getSummaries().observe({ lifecycle }) {
            shoppingListRecyclerViewAdapter.items = it
        }

        root.findViewById<FloatingActionButton>(R.id.add_shopping_list_button)
            .setOnClickListener { onAddListEvent(fragmentViewModel, root) }

        return root
    }

    private fun onAddListEvent(
        fragmentViewModel: MainFragmentViewModel,
        root: View
    ) {
        val dialogView =
            LayoutInflater.from(context).inflate(R.layout.dialog_new_shopping_list_form, null)
        AlertDialog.Builder(context!!)
            .setView(dialogView)
            .setPositiveButton(R.string.bt_text_save) { dialog, _ ->
                val name = dialogView
                    .findViewById<EditText>(R.id.et_new_shopping_list_name)
                    .text
                    .toString()
                fragmentViewModel.onCreateList(name).observe({ lifecycle }) {
                    if (it is Error) {
                        Snackbar.make(
                            root,
                            getString(R.string.dialog_new_shopping_error_feedback),
                            Snackbar.LENGTH_LONG
                        ).show()
                    }
                }
                dialog.dismiss()
            }
            .setNegativeButton(android.R.string.cancel) { dialog, _ -> dialog.dismiss() }
            .show()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnShoppingListSummaryTappedListener) {
            itemTappedListener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        itemTappedListener = null
    }

    interface OnShoppingListSummaryTappedListener {
        fun onTapped(shoppingListSummary: ShoppingListSummary)
    }

}
