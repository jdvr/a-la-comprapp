package es.juandavidvega.alacomprapp.ui.shoppinglist


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.CompoundButton.OnCheckedChangeListener
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import es.juandavidvega.alacomprapp.R
import es.juandavidvega.alacomprapp.domain.model.ShoppingCardProduct
import kotlinx.android.synthetic.main.dialog_new_product_form.view.*
import kotlinx.android.synthetic.main.fragment_product_summary.view.*

class ShoppingListRecyclerViewAdapter(
    initialValues: List<ShoppingCardProduct>,
    private val onProductChangedListener: OnProductChangedListener
) : RecyclerView.Adapter<ShoppingListRecyclerViewAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener = View.OnClickListener { v ->
        val item = v.tag as ShoppingCardProduct
        onProductChangedListener.onChange(item, OnProductChangedListener.Action.EDIT)
    }

    private val onCheckedChangeListener: OnCheckedChangeListener =
        OnCheckedChangeListener { buttonView, isChecked ->
            val item = buttonView.tag as ShoppingCardProduct
            val newStateItem = item.copy(
                checked = isChecked
            )
            onProductChangedListener.onChange(newStateItem, OnProductChangedListener.Action.CHECKED)
        }


    var items = initialValues
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_product_summary, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.fillWith(item, onCheckedChangeListener)
        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = items.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        private val tvName: TextView = mView.textview_product_name
        private val tvQuantity: TextView = mView.textview_prod_quantity
        private val tvPrice: TextView = mView.textview_prod_price
        private val checkbox: CheckBox = mView.checkbox_is_selected
        private val tvProductInfo: TextView = mView.textview_product_info

        fun fillWith(
            shoppingCardProduct: ShoppingCardProduct,
            onCheckedChangeListener: OnCheckedChangeListener
        ) {
            tvName.text = shoppingCardProduct.product.name
            tvQuantity.text = shoppingCardProduct.quantity.toString()
            displayPrice(shoppingCardProduct.product.price)
            configureCheckbox(shoppingCardProduct, onCheckedChangeListener)
            tvProductInfo.text = shoppingCardProduct.product.category
        }

        private fun configureCheckbox(
            shoppingCardProduct: ShoppingCardProduct,
            onCheckedChangeListener: OnCheckedChangeListener
        ) {
            checkbox.isChecked = shoppingCardProduct.checked
            checkbox.setOnCheckedChangeListener(onCheckedChangeListener)
            checkbox.tag = shoppingCardProduct
        }

        private fun displayPrice(price: Double?) =
            if (price == null) {
                tvPrice.visibility = View.GONE
            } else {
                tvPrice.text = tvPrice.context.getString(R.string.price, price, "€")
            }
    }
}
