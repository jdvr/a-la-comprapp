package es.juandavidvega.alacomprapp

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import com.google.android.material.navigation.NavigationView
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import es.juandavidvega.alacomprapp.ui.home.MainFragment
import es.juandavidvega.alacomprapp.ui.home.ShoppingListSummary
import es.juandavidvega.alacomprapp.ui.shoppinglist.ShoppingListActivity

class MainActivity : AppCompatActivity(), MainFragment.OnShoppingListSummaryTappedListener {

    private lateinit var appBarConfiguration: AppBarConfiguration

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)

        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow
            ),
            drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onTapped(shoppingListSummary: ShoppingListSummary) {
        Log.i(LOGGER_TAG, "ShoppingListTapped: ${shoppingListSummary.name}")
        val shoppingListIntent = Intent(this, ShoppingListActivity::class.java).apply {
            putExtra(ShoppingListActivity.SELECTED_SHOPPING_LIST_NAME, shoppingListSummary.name)
        }
        startActivity(shoppingListIntent)
    }

    companion object {
        const val LOGGER_TAG = "MainActivity"
    }
}
