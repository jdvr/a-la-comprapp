package es.juandavidvega.alacomprapp.persistance

import es.juandavidvega.alacomprapp.domain.port.ProductRepository
import es.juandavidvega.alacomprapp.domain.port.ShoppingListRepository

class _RepositoryFactory () {
    var shoppingListRepository: ShoppingListRepository? = null
    var productRepository: ProductRepository? = null

    fun shoppingListRepository(): ShoppingListRepository =
        if (shoppingListRepository == null) {
            shoppingListRepository = InMemoryShoppingListRepository()
            shoppingListRepository!!
        } else {
            shoppingListRepository!!
        }

    fun productRepository(): ProductRepository =
        if (productRepository == null) {
            productRepository = InMemoryProductRepository()
            productRepository!!
        } else {
            productRepository!!
        }
}

object RepositoryFactory {
    val factory = _RepositoryFactory()
    fun createShoppingListRepository(): ShoppingListRepository =
        factory.shoppingListRepository()
    fun createProductRepository(): ProductRepository =
        factory.productRepository()
}