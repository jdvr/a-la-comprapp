package es.juandavidvega.alacomprapp.persistance

import es.juandavidvega.alacomprapp.domain.command.response.Ok
import es.juandavidvega.alacomprapp.domain.command.response.Result
import es.juandavidvega.alacomprapp.domain.model.ShoppingList
import es.juandavidvega.alacomprapp.domain.port.ShoppingListRepository
import io.reactivex.rxjava3.core.BackpressureStrategy
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.subjects.PublishSubject
import java.util.concurrent.ConcurrentHashMap

class InMemoryShoppingListRepository : ShoppingListRepository {

    private val memory = ConcurrentHashMap<String, ShoppingList>()
    private val newElementsFlow: PublishSubject<ShoppingList> = PublishSubject.create()

    override fun create(
        shoppingList: ShoppingList,
        onComplete: (Result) -> Unit
    ) {
        memory[shoppingList.name] = shoppingList
        onComplete(Ok(shoppingList))
        newElementsFlow.onNext(shoppingList)
    }

    override fun delete(shoppingList: ShoppingList, onComplete: (Result) -> Unit) {
        memory.remove(shoppingList.name)
        onComplete(Ok(shoppingList))
    }

    override fun findByName(name: String): ShoppingList? = memory[name]

    override fun update(shoppingList: ShoppingList, onComplete: (Result) -> Unit) {
        create(shoppingList, onComplete)
    }

    override fun getAll(): Flowable<ShoppingList> =
        newElementsFlow.toFlowable(BackpressureStrategy.BUFFER)

    override fun all(): List<ShoppingList> =
        memory.values.toList()

}