package es.juandavidvega.alacomprapp.persistance

import es.juandavidvega.alacomprapp.domain.model.Product
import es.juandavidvega.alacomprapp.domain.model.ShoppingList
import es.juandavidvega.alacomprapp.domain.port.ProductRepository
import io.reactivex.rxjava3.core.BackpressureStrategy
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.subjects.PublishSubject
import java.util.concurrent.ConcurrentHashMap

class InMemoryProductRepository : ProductRepository {

    private val memory = ConcurrentHashMap<String, Product>()
    private val newElementsFlow: PublishSubject<Product> = PublishSubject.create()

    override fun save(product: Product) {
        memory[product.name] = product
        newElementsFlow.onNext(product)
    }

    override fun delete(product: Product) {
        memory.remove(product.name)
    }

    override fun findByName(name: String): Product? = memory[name]

    override fun getAll(): Flowable<Product> =
        newElementsFlow.toFlowable(BackpressureStrategy.BUFFER)

    override fun all(): List<Product> =
        memory.values.toList()

}