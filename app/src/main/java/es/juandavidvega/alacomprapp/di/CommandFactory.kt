package es.juandavidvega.alacomprapp.di

import es.juandavidvega.alacomprapp.domain.port.ProductRepository
import es.juandavidvega.alacomprapp.domain.port.ShoppingListRepository

class CommandFactory(
  private val productRepository: ProductRepository,
  private val shoppingListRepository: ShoppingListRepository
) {

}