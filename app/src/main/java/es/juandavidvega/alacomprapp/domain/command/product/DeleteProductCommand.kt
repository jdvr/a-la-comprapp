package es.juandavidvega.alacomprapp.domain.command.product

import es.juandavidvega.alacomprapp.domain.model.Product
import es.juandavidvega.alacomprapp.domain.port.ProductRepository

class DeleteProductCommand(
    private val productRepository: ProductRepository
) {
    fun execute(product: Product) {
        productRepository.findByName(product.name)?.let {
            productRepository.delete(it)
        } ?: throw IllegalArgumentException("Product ${product.name} does not exists")
    }
}