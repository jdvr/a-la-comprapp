package es.juandavidvega.alacomprapp.domain.command.list

import es.juandavidvega.alacomprapp.domain.command.response.Result
import es.juandavidvega.alacomprapp.domain.model.Product
import es.juandavidvega.alacomprapp.domain.model.ShoppingList
import es.juandavidvega.alacomprapp.domain.port.ShoppingListRepository

class DeleteProductFromShoppingListCommand(
    private val shoppingListRepository: ShoppingListRepository
) {
    fun execute(product: Product, shoppingList: ShoppingList, onComplete: (Result) -> Unit) {
        if (shoppingListRepository.findByName(shoppingList.name) == null) {
            throw IllegalArgumentException("ShoppingList ${shoppingList.name} does not exists")
        }
        shoppingListRepository.update(shoppingList.delete(product), onComplete)
    }
}