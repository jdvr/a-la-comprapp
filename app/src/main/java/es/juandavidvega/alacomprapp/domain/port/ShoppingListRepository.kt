package es.juandavidvega.alacomprapp.domain.port

import es.juandavidvega.alacomprapp.domain.command.response.Result
import es.juandavidvega.alacomprapp.domain.model.ShoppingList
import io.reactivex.rxjava3.core.Flowable

interface ShoppingListRepository {
    fun create(
        shoppingList: ShoppingList,
        onComplete: (Result) -> Unit
    )
    fun delete(shoppingList: ShoppingList, onComplete: (Result) -> Unit)
    fun update(shoppingList: ShoppingList, onComplete: (Result) -> Unit)
    fun findByName(name: String): ShoppingList?
    fun getAll(): Flowable<ShoppingList>
    fun all(): List<ShoppingList>
}
