package es.juandavidvega.alacomprapp.domain.command.list

import es.juandavidvega.alacomprapp.domain.model.ShoppingList
import es.juandavidvega.alacomprapp.domain.port.ShoppingListRepository
import io.reactivex.rxjava3.core.Flowable

class LoadShoppingListCommand(
    private val shoppingListRepository: ShoppingListRepository
) {
    fun execute(): Flowable<ShoppingList> =
        shoppingListRepository.getAll()
}