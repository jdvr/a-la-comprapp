package es.juandavidvega.alacomprapp.domain.command.list

import es.juandavidvega.alacomprapp.domain.command.response.Result
import es.juandavidvega.alacomprapp.domain.model.ShoppingCardProduct
import es.juandavidvega.alacomprapp.domain.model.ShoppingList
import es.juandavidvega.alacomprapp.domain.port.ShoppingListRepository

class CreateShoppingListCommand(
    private val shoppingListRepository: ShoppingListRepository
) {

    fun execute(name: String, products: List<ShoppingCardProduct>, onComplete: (Result) -> Unit) {
        if (shoppingListRepository.findByName(name) != null) {
            throw IllegalArgumentException("ShoppingList $name already exists")
        }
        shoppingListRepository.create(ShoppingList(name, products), onComplete)
    }
}