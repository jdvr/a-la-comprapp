package es.juandavidvega.alacomprapp.domain.model

data class Product (
    val name: String,
    val price: Double?,
    val category: String?
)