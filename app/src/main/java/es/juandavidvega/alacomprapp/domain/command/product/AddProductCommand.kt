package es.juandavidvega.alacomprapp.domain.command.product

import es.juandavidvega.alacomprapp.domain.model.Product
import es.juandavidvega.alacomprapp.domain.port.ProductRepository

class AddProductCommand(
    private val productRepository: ProductRepository
) {
    fun execute(product: Product) {
        if (productRepository.findByName(product.name) != null) {
            throw IllegalArgumentException("Product ${product.name} already exists")
        }
        productRepository.save(product)
    }
}