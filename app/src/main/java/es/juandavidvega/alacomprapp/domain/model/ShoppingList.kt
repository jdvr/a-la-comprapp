package es.juandavidvega.alacomprapp.domain.model

data class ShoppingCardProduct(
    val product: Product,
    val quantity: Int,
    val checked: Boolean
)

class ShoppingList(val name: String, products: List<ShoppingCardProduct>) {

    private val cardProducts: Map<String, ShoppingCardProduct> = products.asMap()

    fun total(): Double = cardProducts.values
        .filter { it.product.price != null }
        .map { it.product.price!! * it.quantity }
        .sum()

    fun checkedTotal(): Double = cardProducts.values
        .filter { it.checked }
        .filter { it.product.price != null }
        .map { it.product.price!! * it.quantity }
        .sum()

    fun pendingProducts(): Int = cardProducts.values
        .filter { !it.checked }
        .size

    fun products() = cardProducts.values.toList()

    fun count(): Int = cardProducts.size

    fun check(product: Product): ShoppingList =
        with(cardProducts[product.name]) {
            this?.let {
                ShoppingList(
                    name,
                    cardProducts.markCheck(it).values.toList()
                )
            } ?: throw NoSuchElementException("Missing product ${product.name}")
        }

    fun add(product: Product, quantity: Int): ShoppingList =
        if (cardProducts.containsKey(product.name)) {
            this
        } else {
            ShoppingList(
                name,
                cardProducts.plus(
                    product.name to ShoppingCardProduct(
                        product,
                        quantity,
                        false
                    )
                ).values.toList()
            )
        }

    fun delete(product: Product): ShoppingList =
        if (!cardProducts.containsKey(product.name)) {
            this
        } else {
            ShoppingList(
                name,
                cardProducts.values.filter { it.product != product }
            )
        }

    private fun List<ShoppingCardProduct>.asMap(): Map<String, ShoppingCardProduct> =
        this.groupBy { it.product.name }.mapValues { it.value.first() }

    private fun Map<String, ShoppingCardProduct>.markCheck(cardProduct: ShoppingCardProduct): Map<String, ShoppingCardProduct> =
        this.plus(
            cardProduct.product.name to cardProduct.copy(checked = true)
        )

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ShoppingList

        if (name != other.name) return false
        if (cardProducts != other.cardProducts) return false

        return true
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + cardProducts.hashCode()
        return result
    }


}