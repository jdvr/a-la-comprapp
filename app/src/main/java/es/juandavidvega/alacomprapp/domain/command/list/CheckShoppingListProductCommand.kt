package es.juandavidvega.alacomprapp.domain.command.list

import es.juandavidvega.alacomprapp.domain.command.response.Result
import es.juandavidvega.alacomprapp.domain.model.Product
import es.juandavidvega.alacomprapp.domain.port.ShoppingListRepository

class CheckShoppingListProductCommand(
    private val shoppingListRepository: ShoppingListRepository
) {

    fun execute(
        product: Product,
        shoppingListName: String,
        onComplete: (Result) -> Unit
    ) {
        val dbShoppingList = shoppingListRepository.findByName(shoppingListName)
            ?: throw IllegalArgumentException("ShoppingList $shoppingListName does not exists")
        shoppingListRepository.update(dbShoppingList.check(product), onComplete)
    }

}