package es.juandavidvega.alacomprapp.domain.command.response

import java.lang.IllegalArgumentException
import java.lang.RuntimeException

sealed class Result

data class Ok<T>(val data: T) : Result()
data class Error(val message: String, val exception: RuntimeException) : Result()