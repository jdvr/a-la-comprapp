package es.juandavidvega.alacomprapp.domain.command.list

import es.juandavidvega.alacomprapp.domain.command.response.Result
import es.juandavidvega.alacomprapp.domain.model.Product
import es.juandavidvega.alacomprapp.domain.port.ProductRepository
import es.juandavidvega.alacomprapp.domain.port.ShoppingListRepository

class AddProductToShoppingListCommand (
    private val productRepository: ProductRepository,
    private val shoppingListRepository: ShoppingListRepository
) {

    fun execute(
        product: Product,
        quantity: Int,
        shoppingListName: String,
        onComplete: (Result) -> Unit
    ) {
        val listFromRepository = shoppingListRepository.findByName(shoppingListName)
            ?: throw IllegalArgumentException("ShoppingList $shoppingListName does not exists")
        if (productRepository.findByName(product.name) == null) {
            productRepository.save(product)
        }
        val withProduct = listFromRepository.add(product, quantity)
        shoppingListRepository.update(withProduct, onComplete)
    }

}