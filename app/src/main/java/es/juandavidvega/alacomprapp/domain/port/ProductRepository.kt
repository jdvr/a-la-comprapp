package es.juandavidvega.alacomprapp.domain.port

import es.juandavidvega.alacomprapp.domain.model.Product
import io.reactivex.rxjava3.core.Flowable

interface ProductRepository {
    fun save(product: Product)
    fun delete(product: Product)
    fun findByName(name: String): Product?
    fun getAll(): Flowable<Product>
    fun all(): List<Product>
}