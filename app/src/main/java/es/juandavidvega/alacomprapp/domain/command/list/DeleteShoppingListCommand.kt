package es.juandavidvega.alacomprapp.domain.command.list

import es.juandavidvega.alacomprapp.domain.command.response.Result
import es.juandavidvega.alacomprapp.domain.model.ShoppingList
import es.juandavidvega.alacomprapp.domain.port.ShoppingListRepository

class DeleteShoppingListCommand(
    private val shoppingListRepository: ShoppingListRepository
) {
    fun execute(shoppingList: ShoppingList, onComplete: (Result) -> Unit) {
        shoppingListRepository.delete(shoppingList, onComplete)
    }
}