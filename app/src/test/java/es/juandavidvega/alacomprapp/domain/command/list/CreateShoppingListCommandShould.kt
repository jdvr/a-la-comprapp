package es.juandavidvega.alacomprapp.domain.command.list

import assertk.assertThat
import assertk.assertions.hasClass
import assertk.assertions.isFailure
import assertk.assertions.isInstanceOf
import es.juandavidvega.alacomprapp.domain.command.response.Ok
import es.juandavidvega.alacomprapp.domain.command.response.Result
import es.juandavidvega.alacomprapp.domain.model.ShoppingCardProduct
import es.juandavidvega.alacomprapp.domain.model.ShoppingList
import es.juandavidvega.alacomprapp.domain.mother.EMPTY_ON_COMPLETED
import es.juandavidvega.alacomprapp.domain.port.ShoppingListRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.Test


class CreateShoppingListCommandShould {

    @Test
    fun `creates a new ShoppingList when given a name and a list of product`() {
        val name = "newShoppingList"
        val products = emptyList<ShoppingCardProduct>()
        val shoppingListRepository = mockk<ShoppingListRepository>()
        val anyOnCompletedListener = { _: Result -> Unit }
        every { shoppingListRepository.findByName(name) } returns null
        every { shoppingListRepository.create(ShoppingList(name, products), anyOnCompletedListener) } returns Unit

        CreateShoppingListCommand(shoppingListRepository)
            .execute(name, products, anyOnCompletedListener)

        verify { shoppingListRepository.create(ShoppingList(name, products), anyOnCompletedListener) }
    }

    @Test
    fun `Fail when given an existing name`() {
        val name = "existingName"
        val products = emptyList<ShoppingCardProduct>()
        val shoppingListRepository = mockk<ShoppingListRepository>()

        every { shoppingListRepository.findByName(name) } returns ShoppingList(name, products)
        every { shoppingListRepository.create(ShoppingList(name, products), EMPTY_ON_COMPLETED) } returns Unit

        assertThat {
            CreateShoppingListCommand(shoppingListRepository)
                .execute(name, products, EMPTY_ON_COMPLETED)
        }.isFailure().hasClass(IllegalArgumentException::class.java)

        verify(exactly = 0) { shoppingListRepository.create(
            ShoppingList(name, products),
            EMPTY_ON_COMPLETED
        ) }
    }

}