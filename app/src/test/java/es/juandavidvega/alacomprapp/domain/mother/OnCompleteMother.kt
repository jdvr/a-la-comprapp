package es.juandavidvega.alacomprapp.domain.mother

import es.juandavidvega.alacomprapp.domain.command.response.Result

val EMPTY_ON_COMPLETED = {_: Result -> Unit}