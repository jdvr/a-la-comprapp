package es.juandavidvega.alacomprapp.domain.command.list

import assertk.assertions.hasClass
import assertk.assertions.isFailure
import es.juandavidvega.alacomprapp.domain.model.Product
import es.juandavidvega.alacomprapp.domain.model.ShoppingCardProduct
import es.juandavidvega.alacomprapp.domain.model.ShoppingList
import es.juandavidvega.alacomprapp.domain.mother.EMPTY_ON_COMPLETED
import es.juandavidvega.alacomprapp.domain.port.ShoppingListRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.Test

class CheckShoppingListProductCommandShould {

    @Test
    fun `Mark checked product on given card`() {
        val product = Product("newProduct", null, null)
        val quantity = 3
        val shoppingList = ShoppingList("anyName", listOf(ShoppingCardProduct(product, quantity, false)))
        val expectedShoppingList = ShoppingList(
            "anyName",
            listOf(ShoppingCardProduct(product, quantity, true))
        )

        val shoppingListRepository = mockk<ShoppingListRepository>(relaxed = true)

        every { shoppingListRepository.findByName(shoppingList.name) } returns shoppingList

        CheckShoppingListProductCommand(shoppingListRepository)
            .execute(product, shoppingList.name, EMPTY_ON_COMPLETED)

        verify { shoppingListRepository.update(expectedShoppingList, EMPTY_ON_COMPLETED) }
    }

    @Test
    fun `Fail when ShoppingList does not exists`() {
        val shoppingList = ShoppingList("anyName", emptyList())

        val shoppingListRepository = mockk<ShoppingListRepository>(relaxed = true)

        every { shoppingListRepository.findByName(shoppingList.name) } returns null

        assertk.assertThat {
            CheckShoppingListProductCommand(shoppingListRepository)
                .execute(Product("newProduct", null, null), shoppingList.name, EMPTY_ON_COMPLETED)
        }.isFailure().hasClass(IllegalArgumentException::class.java)
    }
}
