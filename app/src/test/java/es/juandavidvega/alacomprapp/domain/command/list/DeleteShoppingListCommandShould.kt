package es.juandavidvega.alacomprapp.domain.command.list

import es.juandavidvega.alacomprapp.domain.model.ShoppingList
import es.juandavidvega.alacomprapp.domain.mother.EMPTY_ON_COMPLETED
import es.juandavidvega.alacomprapp.domain.port.ShoppingListRepository
import io.mockk.mockk
import io.mockk.verify
import org.junit.Test

class DeleteShoppingListCommandShould {

    @Test
    fun `Delete product from given card`() {
        val shoppingListWithProduct = ShoppingList(
            "anyName",
            emptyList()
        )

        val shoppingListRepository = mockk<ShoppingListRepository>(relaxed = true)

        DeleteShoppingListCommand(shoppingListRepository)
            .execute(shoppingListWithProduct, EMPTY_ON_COMPLETED)

        verify { shoppingListRepository.delete(shoppingListWithProduct, EMPTY_ON_COMPLETED) }
    }

}