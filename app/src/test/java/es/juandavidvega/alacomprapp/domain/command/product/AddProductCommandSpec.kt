package es.juandavidvega.alacomprapp.domain.command.product

import assertk.assertThat
import assertk.assertions.hasClass
import assertk.assertions.isFailure
import es.juandavidvega.alacomprapp.domain.model.Product
import es.juandavidvega.alacomprapp.domain.port.ProductRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.Test


class AddProductCommandShould {

    @Test
    fun `store product on database`() {
        val product = Product("anyName", null, null)
        val productRepository = mockk<ProductRepository>()
        every { productRepository.findByName(product.name) } returns null
        every { productRepository.save(product) } returns Unit

        AddProductCommand(productRepository)
            .execute(product)

        verify { productRepository.save(product) }
    }

    @Test
    fun `fail when product already exist`() {
        val product = Product("anyName", null, null)

        val productRepository = mockk<ProductRepository>()

        every { productRepository.findByName(product.name) } returns product

        assertThat {
            AddProductCommand(productRepository)
                .execute(product)
        }.isFailure().hasClass(IllegalArgumentException::class.java)

        verify(exactly = 0) { productRepository.save(product) }
    }
}