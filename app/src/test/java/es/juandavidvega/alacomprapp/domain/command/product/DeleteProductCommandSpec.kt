package es.juandavidvega.alacomprapp.domain.command.product

import assertk.assertThat
import assertk.assertions.hasClass
import assertk.assertions.isFailure
import es.juandavidvega.alacomprapp.domain.model.Product
import es.juandavidvega.alacomprapp.domain.port.ProductRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.Test


class DeleteProductCommandShould {

    @Test
    fun `delete product from database if exists`() {
        val product = Product("anyName", null, null)
        val productRepository = mockk<ProductRepository>()
        every { productRepository.findByName(product.name) } returns product
        every { productRepository.delete(product) } returns Unit

        DeleteProductCommand(productRepository)
            .execute(product)

        verify { productRepository.delete(product) }
    }

    @Test
    fun `fail when product does not exist`() {
        val product = Product("anyName", null, null)

        val productRepository = mockk<ProductRepository>()

        every { productRepository.findByName(product.name) } returns null
        every { productRepository.delete(product) } returns Unit

        assertThat {
            DeleteProductCommand(productRepository)
                .execute(product)
        }.isFailure().hasClass(IllegalArgumentException::class.java)

        verify(exactly = 0) { productRepository.delete(product) }
    }

}