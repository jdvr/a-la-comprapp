package es.juandavidvega.alacomprapp.domain.command.list

import assertk.assertThat
import assertk.assertions.isSameAs
import es.juandavidvega.alacomprapp.domain.model.ShoppingList
import es.juandavidvega.alacomprapp.domain.port.ShoppingListRepository
import io.mockk.every
import io.mockk.mockk
import io.reactivex.rxjava3.core.Flowable
import org.junit.Test

class LoadShoppingListCommandShould {

    @Test
    fun `return an observable stream of user shopping list`() {
        val observableShoppingList = Flowable.just(ShoppingList("anyName", emptyList()))

        val shoppingListRepository = mockk<ShoppingListRepository>(relaxed = true)

        every { shoppingListRepository.getAll() } returns observableShoppingList

        val response = LoadShoppingListCommand(shoppingListRepository).execute()

        assertThat(response).isSameAs(observableShoppingList)

    }
}