package es.juandavidvega.alacomprapp.domain.command.list

import assertk.assertThat
import assertk.assertions.hasClass
import assertk.assertions.isFailure
import es.juandavidvega.alacomprapp.domain.model.Product
import es.juandavidvega.alacomprapp.domain.model.ShoppingCardProduct
import es.juandavidvega.alacomprapp.domain.model.ShoppingList
import es.juandavidvega.alacomprapp.domain.mother.EMPTY_ON_COMPLETED
import es.juandavidvega.alacomprapp.domain.port.ProductRepository
import es.juandavidvega.alacomprapp.domain.port.ShoppingListRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.Test

class AddProductToShoppingListCommandShould {

    @Test
    fun `Add product to given card with specified quantity`() {
        val shoppingList = ShoppingList("anyName", emptyList())
        val product = Product("newProduct", null, null)
        val quantity = 3
        val shoppingListWithProduct = ShoppingList(
            "anyName",
            listOf(ShoppingCardProduct(product, quantity, false))
        )

        val productRepository = mockk<ProductRepository>(relaxed = true)
        val shoppingListRepository = mockk<ShoppingListRepository>(relaxed = true)

        every { productRepository.findByName(product.name) } returns product
        every { shoppingListRepository.findByName(shoppingList.name) } returns shoppingList

        AddProductToShoppingListCommand(productRepository, shoppingListRepository)
            .execute(product, quantity, shoppingList.name, EMPTY_ON_COMPLETED)

        verify { shoppingListRepository.update(shoppingListWithProduct, EMPTY_ON_COMPLETED) }
    }

    @Test
    fun `Create and then add product to given card with specified quantity`() {
        val shoppingList = ShoppingList("anyName", emptyList())
        val product = Product("newProduct", null, null)
        val quantity = 3
        val shoppingListWithProduct = ShoppingList(
            "anyName",
            listOf(ShoppingCardProduct(product, quantity, false))
        )

        val productRepository = mockk<ProductRepository>(relaxed = true)
        val shoppingListRepository = mockk<ShoppingListRepository>(relaxed = true)

        every { productRepository.findByName(product.name) } returns null
        every { shoppingListRepository.findByName(shoppingList.name) } returns shoppingList

        AddProductToShoppingListCommand(productRepository, shoppingListRepository)
            .execute(product, quantity, shoppingList.name, EMPTY_ON_COMPLETED)

        verify { shoppingListRepository.update(shoppingListWithProduct, EMPTY_ON_COMPLETED) }
        verify { productRepository.save(product) }
    }

    @Test
    fun `Fail when ShoppingList does not exists`() {
        val shoppingList = ShoppingList("anyName", emptyList())

        val productRepository = mockk<ProductRepository>(relaxed = true)
        val shoppingListRepository = mockk<ShoppingListRepository>(relaxed = true)

        every { shoppingListRepository.findByName(shoppingList.name) } returns null

        assertThat {
            AddProductToShoppingListCommand(productRepository, shoppingListRepository)
                .execute(
                    Product("newProduct", null, null),
                    3,
                    shoppingList.name,
                    EMPTY_ON_COMPLETED
                )
        }.isFailure().hasClass(IllegalArgumentException::class.java)
    }
}